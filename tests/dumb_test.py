# pylint: disable=import-error
import tonos_ts4.ts4 as ts4

import pathlib

from pytest import fixture


eq = ts4.eq


@fixture
def contract(pytestconfig):
    rootpath: pathlib.Path = pytestconfig.rootpath
    ts4.init(rootpath.joinpath('contracts/'), verbose = False)
    return ts4.BaseContract('dumb_contract', {})


def test_hello(contract):
    answer = contract.call_getter('renderHelloWorld')
    assert eq('helloWorld', answer)
