pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract dumb_contract {

    constructor() public {
        tvm.accept();
    }

    function renderHelloWorld () public pure returns (string) {
        return 'helloWorld';
    }
}
